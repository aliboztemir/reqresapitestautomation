package step_definitions;

import static org.junit.Assert.*;

import org.junit.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import test_resources.Utilities;
import test_resources.ReqresReqSpecs;
import test_resources.ReqresRespSpecs;

public class ReqresStepDefinition {

    private RequestSpecification reqresReqSpec = null;
    private Response reqresResponse = null;
    private static String task_id = "";
    private static String access_token = "";
    private static String user_id = "";
    private static String single_user_id = "";


    public static String getTaskId() {
        return task_id;
    }

    public static String getAccessToken() {
        return access_token;
    }


    @When("{word} request is sent as {word} HTTP request")
    public void request_is_sent_as_http_request(String api, String requestType) {
        reqresResponse = Utilities.getResource(reqresReqSpec, api, requestType);

        if (api.equalsIgnoreCase("LoginAPI")) {
            access_token = Utilities.getResponseValue(reqresResponse, "access_token");
        }
        if (api.equalsIgnoreCase("AddTaskAPI")) {
            task_id = Utilities.getResponseValue(reqresResponse, "data.id");
        }

        if (api.equalsIgnoreCase("CreateUserAPI")) {
            user_id = Utilities.getResponseValue(reqresResponse, "id");
            System.out.println("..........    user_id " + user_id);
        }

        if (api.equalsIgnoreCase("ListUsersAPI")) {
            single_user_id = Utilities.getResponseValue(reqresResponse, "data[0].id");
            System.out.println("..........    single_user_id " + single_user_id);
        }
    }

    @Then("success/fail response is sent back with status code {int}")
    public void response_is_sent_back_with_status_code(int code) {
        // Then() part of request
        reqresResponse = ReqresRespSpecs.applyRespSpec(reqresResponse, code);
    }

    @Then("{word} value in response is {string}")
    public void value_in_response_is(String data, String expectedValue) {
        // Retrieve actual value from response
        String actualValue = Utilities.getResponseValue(reqresResponse, data);

        // Check actual data value matches expected data value in response
        assertEquals(actualValue, expectedValue);
    }

    @Then("place_id maps to {word} {string} using GetPlaceAPI")
    public void place_id_maps_to_data_using_get_place_api(String data, String expectedValue) {
        // Get data from GetUserAPI response
        String actualValue = Utilities.getResponseValue(reqresResponse, data);

        // Check actual data value matches expected data value in response
        assertEquals(actualValue, expectedValue);
    }

    @Then("length of {word} value in response must be greater than zero")
    public void length_of_id_value_in_response_must_be_greater_than_zero(String data) {
        // Get data from GetUserAPI response
        String actualValue = Utilities.getResponseValue(reqresResponse, data);
        assertTrue(actualValue.length() > 0);
    }

    @Then("response time must be less than {int} ms")
    public void response_time_must_be_less_than_ms(Integer responseTime) {
        long actualValue = ReqresRespSpecs.applyRespTimeSpec(reqresResponse);
        assertTrue(actualValue < responseTime);
    }


    @Then("Validate that the {word} of the last created Task is in the list")
    public void validate_that_the_id_of_the_last_created_task_is_in_the_list(String data) {
        // Retrieve actual value from response
        Assert.assertTrue(Utilities.extractValueInResponseItemList(reqresResponse, data));
    }


    @Given("a {word} ListUsersAPI request")
    public void a_valid_list_users_api_request(String data) {
        reqresReqSpec = ReqresReqSpecs.getListUsersReqSpec(data);
    }


    @Given("a {word} SingleUserAPI request")
    public void a_valid_single_user_api_request(String data) {
        reqresReqSpec = ReqresReqSpecs.getSingleUser(single_user_id, data);
    }

    @Given("a valid CreateUserAPI payload with data {string} {string}")
    public void a_valid_create_user_api_payload_with_data(String name, String job) {
        reqresReqSpec = ReqresReqSpecs.createUserReqSpec(name, job);
    }

    @Given("a valid PutUserAPI payload with data {string} {string}")
    public void a_valid_put_user_api_payload_with_data(String name, String job) {
        reqresReqSpec = ReqresReqSpecs.putUserReqSpec(user_id, name, job);
    }

    @Given("a valid PatchUserAPI payload with data {string} {string}")
    public void a_valid_patch_user_api_payload_with_data(String name, String job) {
        reqresReqSpec = ReqresReqSpecs.patchUserReqSpec(user_id, name, job);
    }

    @Given("a valid DeleteUserAPI request")
    public void a_valid_delete_user_api_request() {
        reqresReqSpec = ReqresReqSpecs.deleteUserReqSpec(single_user_id);
    }
}
