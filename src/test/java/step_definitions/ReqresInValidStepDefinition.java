package step_definitions;

import io.cucumber.java.en.When;
import io.cucumber.java.en.Given;

public class ReqresInValidStepDefinition {

	@Given("a invalid CreateUserAPI payload with data {string} {string}")
	public void a_invalid_create_user_api_payload_with_data(String string, String string2) {
		// Write code here that turns the phrase above into concrete actions
		throw new io.cucumber.java.PendingException();
	}

	@When("CreateUserAPI request is sent as Post HTTP request with invalid data")
	public void create_user_api_request_is_sent_as_post_http_request_with_invalid_data() {
		// Write code here that turns the phrase above into concrete actions
		throw new io.cucumber.java.PendingException();
	}

	@Given("a invalid CreateUserAPI payload with data null null")
	public void a_invalid_create_user_api_payload_with_data_null_null() {
		// Write code here that turns the phrase above into concrete actions
		throw new io.cucumber.java.PendingException();
	}

	@Given("a invalid CreateUserAPI payload with data {string}")
	public void a_invalid_create_user_api_payload_with_data(String string) {
		// Write code here that turns the phrase above into concrete actions
		throw new io.cucumber.java.PendingException();
	}

	@When("SingleUserAPI request is sent as Get HTTP request with invalid data")
	public void single_user_api_request_is_sent_as_get_http_request_with_invalid_data() {
		// Write code here that turns the phrase above into concrete actions
		throw new io.cucumber.java.PendingException();
	}

	@Given("a invalid PutUserAPI payload with data {string}")
	public void a_invalid_put_user_api_payload_with_data(String string) {
		// Write code here that turns the phrase above into concrete actions
		throw new io.cucumber.java.PendingException();
	}

	@When("PutUserAPI request is sent as Put HTTP request with invalid data")
	public void put_user_api_request_is_sent_as_put_http_request_with_invalid_data() {
		// Write code here that turns the phrase above into concrete actions
		throw new io.cucumber.java.PendingException();
	}

	@Given("a invalid PutUserAPI payload with data {string} {string}")
	public void a_invalid_put_user_api_payload_with_data(String string, String string2) {
		// Write code here that turns the phrase above into concrete actions
		throw new io.cucumber.java.PendingException();
	}

	@Given("a invalid PutUserAPI payload with data null")
	public void a_invalid_put_user_api_payload_with_data_null() {
		// Write code here that turns the phrase above into concrete actions
		throw new io.cucumber.java.PendingException();
	}

	@Given("a invalid PatchUserAPI payload with data null")
	public void a_invalid_patch_user_api_payload_with_data_null() {
		// Write code here that turns the phrase above into concrete actions
		throw new io.cucumber.java.PendingException();
	}

	@When("PatchUserAPI request is sent as Put HTTP request with invalid data")
	public void patch_user_api_request_is_sent_as_put_http_request_with_invalid_data() {
		// Write code here that turns the phrase above into concrete actions
		throw new io.cucumber.java.PendingException();
	}

	@Given("a invalid DeleteUserAPI request")
	public void a_invalid_delete_user_api_request() {
		// Write code here that turns the phrase above into concrete actions
		throw new io.cucumber.java.PendingException();
	}

	@When("DeleteUserAPI request is sent as Delete HTTP request with invalid data")
	public void delete_user_api_request_is_sent_as_delete_http_request_with_invalid_data() {
		// Write code here that turns the phrase above into concrete actions
		throw new io.cucumber.java.PendingException();
	}
}
